#include "stdio.h"
#include "stdafx.h"
#include "Codes.h"
#include "Resource.h"


char szData[5024];
int m_iCode = 1;

void Encode( char & cChar );
void Decode( char & cChar );


wchar_t *ANSIToUnicode( const char* str )
{
   int textlen ;
   static wchar_t result[1024];
   textlen = MultiByteToWideChar( CP_ACP, 0, str,-1, NULL,0 ); 
   memset(result, 0, sizeof(char) * ( textlen + 1 ) );
   MultiByteToWideChar(CP_ACP, 0,str,-1,(LPWSTR)result,textlen ); 
   return result; 
}

char *UnicodeToANSI( const wchar_t* str )
{
     static char result[1024];
     int textlen;
     textlen = WideCharToMultiByte( CP_ACP, 0, str, -1, NULL, 0, NULL, NULL );
     memset(result, 0, sizeof(char) * ( textlen + 1 ) );
     WideCharToMultiByte( CP_ACP, 0, str, -1, result, textlen, NULL, NULL );
     return result;
}

void ReadData(void)
{
	memset(szData, 0, sizeof(szData));
	FILE *pFile;
	pFile = fopen("UserData.db", "rt");
	if(!pFile)
		return;

	char szBuff[1024];

	while(!feof(pFile))
	{
		fgets(szBuff, sizeof(szBuff)-1, pFile);
		sprintf(szData, "%s%s",szData, szBuff);
	}

	fclose(pFile);

	for( int i =0; i<strlen(szData); i++ )
	{
		Decode(szData[ i]);
	}

	HWND hwnd = FindWindow(NULL, L"Encode");
	SetDlgItemText(hwnd, IDC_EDIT1, ANSIToUnicode(szData) );
}
void SaveData( char *pszText )
{


	FILE *pFile;
	pFile = fopen( "UserData.db"  ,"w");

	if( !pFile )
		return;

	for( int i=0; i< strlen( pszText ); i++ )
	{
		Encode(pszText[ i ] );
	}

	fprintf( pFile, "%s", pszText);


	fclose( pFile );
	free( pszText );

}

void Encode( char & cChar )
{
	/*if(cChar == '\n')
		return;

	if(cChar == '\0')
		return;*/

	cChar += m_iCode;
}

void Decode( char & cChar )
{
	/*if(cChar == '\n')
		return;

	if(cChar == '\0')
		return;*/

	cChar -= m_iCode;
}