#ifndef _CODES_H
#define _CODES_H

void ReadData(void);
void SaveData( char *pszText );
char *UnicodeToANSI( const wchar_t* str );

#endif